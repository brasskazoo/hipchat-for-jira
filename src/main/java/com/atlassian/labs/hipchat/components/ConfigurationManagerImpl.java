package com.atlassian.labs.hipchat.components;

import com.atlassian.hipchat.plugins.api.config.HipChatConfigurationManager;

import static com.google.common.base.Preconditions.checkNotNull;

public final class ConfigurationManagerImpl implements ConfigurationManager
{
    private final HipChatConfigurationManager hipChatConfigurationManager;

    public ConfigurationManagerImpl(HipChatConfigurationManager hipChatConfigurationManager)
    {
        this.hipChatConfigurationManager = checkNotNull(hipChatConfigurationManager);
    }

    public String getHipChatApiToken()
    {
        return hipChatConfigurationManager.getApiToken().getOrNull();
    }

    public void updateHipChatApiToken(String token)
    {
        hipChatConfigurationManager.setApiToken(token);
    }
}
