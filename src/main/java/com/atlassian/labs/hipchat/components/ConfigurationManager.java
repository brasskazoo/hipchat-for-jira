package com.atlassian.labs.hipchat.components;

/**
 * This interface is API. Please make only additive changes to it until the next X.0 release
 * @deprecated since 1.2, use the APIs in the {@code com.atlassian.hipchat.plugins.api} package.
 */
@Deprecated
public interface ConfigurationManager {

    public String getHipChatApiToken();

    public void updateHipChatApiToken(String token);
}